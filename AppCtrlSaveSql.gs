var FOLDER_ID = '1pU_GMDpLfvzvIp9TBIpvb5C0kMgxh_og'; // https://drive.google.com/drive/u/1/folders/1pU_GMDpLfvzvIp9TBIpvb5C0kMgxh_og
var PREFIX = Utilities.formatDate(new Date(), "GMT+7", "yyMMddHHmmss");

function saveSqlToDraftFolder(dbs) {
  var folder = DriveApp.getFolderById(FOLDER_ID);

  // Save new file to folder
  for(var dbName in dbs) {
    var db = dbs[dbName];
    var fileName = db.name + ".sql";
    folder.createFile(PREFIX + '_' + fileName, db.sql);
  }
}
