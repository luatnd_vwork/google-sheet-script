/**
 * YOUR_GLOBAL_CONST
 * Usage:
 *    In any .gs file, just call:
 *      var app_env = YOUR_GLOBAL_CONST['Env'];
 *
 * @type {string}
 */
var YOUR_GLOBAL_CONST = {
  Env: 'dev',
  FooConst: 'You can put all app config here',
};