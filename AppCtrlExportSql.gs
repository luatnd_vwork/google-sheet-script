// -----------------------------------------------
function showDlg__ExportMySqlCreateTableQuery() {
  openDialog__MySqlCreateTableQueries('CREATE TABLE SQL', getExportMySqlCreateTableQueries());
}

function openDialog__MySqlCreateTableQueries(appTitle, ctrlData) {
  var html = getHtmlOutput__MySqlCreateTableQueries(ctrlData);

  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
      .showModalDialog(html.setWidth(850).setHeight(550), appTitle);

  saveSqlToDraftFolder(ctrlData.dbs);
}

function getHtmlOutput__MySqlCreateTableQueries(ctrlData) {
  var t = HtmlService.createTemplateFromFile('Dialog_CreateTableSql');

  t.dbs = ctrlData.dbs;

  return t.evaluate();
}


// -----------------------------------------------
function showDlg__Export_Kafka2Mongo_SchemaMap() {
  openDialog__SchemaMap('kafka2mongo data (realtime)', getExportData_SchemaMap());
}
function showDlg__Export_Kafka2Mongo_SchemaMap_FromCache() {
  openDialog__SchemaMap('kafka2mongo data (from cache)', getExportData_SchemaMap_FromCache());
}

function openDialog__SchemaMap(appTitle, ctrlData) {
  var html = getHtmlOutput__SchemaMap(ctrlData);

  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
      .showModalDialog(html.setWidth(960).setHeight(550), appTitle);
}

function getHtmlOutput__SchemaMap(ctrlData) {
  var t = HtmlService.createTemplateFromFile('Dialog_SchemaMap');

  t.ctrlData = ctrlData;

  return t.evaluate();
}

// -----------------------------------------------
function showDlg__Create_From_Cache() {
  SpreadsheetApp.getUi().alert('Please use Extras > Export kafka-to-mongo SCHEMA_MAP to generate cache first, then you can use this menu items.');
}
function showDlg__Create_Indexes_Script() {
  SpreadsheetApp.getUi().alert('Please use Extras > Export kafka-to-mongo SCHEMA_MAP');
}