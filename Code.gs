function onOpen() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var menuEntries = [
    {name: "Utils live:", functionName: "doNothing"},
    {name: "• Export kafka-to-mongo SCHEMA_MAP", functionName: "showDlg__Export_Kafka2Mongo_SchemaMap"},
    null, // Line separator
    {name: "Utils from cache:", functionName: "showDlg__Create_From_Cache"},
    {name: "• Export kafka-to-mongo SCHEMA_MAP (from cache)", functionName: "showDlg__Export_Kafka2Mongo_SchemaMap_FromCache"},
    {name: "• Export SQL script (from cache)", functionName: "showDlg__ExportMySqlCreateTableQuery"},
    null, // Line separator
    {name: "Create Indexes script", functionName: "showDlg__Create_Indexes_Script"}
  ];
  ss.addMenu("Extras", menuEntries);
}


/*
* API entry point
*/
function doGet(e) {
  // Decide which action you do:
  var response = {a:1};

  // Response JSON
  return HtmlService.createHtmlOutput(JSON.stringify(response));
}

function doNothing(){}

function testIsArray() {
  var a  = [1,2,2,3,3];
  var b = {a: 1, b:2};

  var c = typeof a;
  var d = typeof b
  var e = Array.isArray(a);
  var f = Array.isArray(b);

  var stop = true;
}

