/**
 * TODO:
 *  [ ] Allow config some const from UI with default value
 *  [ ] Move all configuration const to the Config/App.gs
 */


var SCHEMA_PREFIX = 'roma_';
var REPLICATION_TABLE_PREFIX = 'rep_';
var CREATE_REP_INDEXES_ONLY = false;

var DB_DESIGN_INDEX_COLS_POS = {
  dbName: "A",
  tableComment: "B",
  tableName: "C",
  tableLink: "D",
};

var BASE_TABLE_COLS_POS = {
  name: 'A',
  type: 'B',
  needIndex: 'D',
  comment: 'E',
  desc: 'F',
  repInfo: 'K',
};

var REP_TABLE_COLS_POS = {
  name: 'A',
  type: 'B',
  needIndex: 'C',
  comment: 'D',
  desc: 'E',
  repInfo: 'F',
};

/**
 * We always index these fields
 */
var DEFAULT_INDEXES_FIELDS = [
  '_ref',
  'created_at',
  'created_by',
  'updated_at',
  'updated_by',
  'created_source',
];


/**
 * Return xxx.
 *
 * If you modify getExportData_SchemaMap()
 * Then plz mod  getExportData_SchemaMap_FromCache() too
 *
 * @return {Object} Schema_map and some useful data.
 */
function getExportData_SchemaMap() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var tableRange = ss.getRange("Index!A2:D200");


  // Get list of tables (each table is 1 sheet url)
  var lst = getDbListFromRange(tableRange, DB_DESIGN_INDEX_COLS_POS);
  var dbs = lst.dbs;
  var schemaMapCache = lst.schemaMapCache;

  // Init empty schema_map:
  var schema_map = {};
  var rep_only_dbs = {}; // Db that contain `rep_` tables only
  var table_path = {};  // Build index: [table_name => db_name.table_name]
  var table_was_edited = false;

  var count = 0;
  // Async process: read tables and store definition in dbs
  for(var dbk in dbs) {
    var db = dbs[dbk];
    var dbName = db.name;
    var tables = db.tables;

    rep_only_dbs[dbk] = {
      name: db.name,
      comment: db.comment,
      tables: {}
    };

    var hasReptable = false;

    for (var tableName in tables) {
      count++;

      /*
      // Debug only 10 tables in dev mode
      if (!((45 < count) && (count < 60))) {
        continue;
      }
      */

      //// Only care about rep_* tables <=> tableName not start with `rep_`
      var isRepTable = tableName.indexOf(REPLICATION_TABLE_PREFIX) === 0;

      if (!isRepTable) {
        table_path[tableName] = dbName + '.' + tableName;
      //  continue; // We still need to read non rep_table, so do not `continue` right now.
      } else {
        hasReptable = true;
      }

      var table = tables[tableName];

      // Ignore no link
      if (table.link.length == 0) {
        table.fields = [];
      }
      else {
        // Try to read from cache
        if (!table.cache_expired) {
          try {
            var tableCache = schemaMapCache["data"][dbk]["tables"][tableName];
          } catch (e) {
            Logger.log({e: e});
          }

          table.fields = typeof tableCache !== 'undefined'? tableCache.fields : {};
        }

        if (Object.keys(table.fields).length > 0) {}
        else {
          // Read newest version
          table_was_edited = true; // mark cache as expired

          var sheet = getNetworkSheet(table.link, 0);
          var range = 'A5:K100';
          var virtualColsPosition = isRepTable ? REP_TABLE_COLS_POS: BASE_TABLE_COLS_POS;

          // Handle some exception case:
          try {

            // Case: rep_* structure is the same as base table structure
            var baseTableUniqueHeaderCol = sheet.getRange('C4').getValues();
            var textC4 = baseTableUniqueHeaderCol[0][0];
            if (isRepTable && textC4 === 'Unique') {
              virtualColsPosition = BASE_TABLE_COLS_POS;

              Logger.log({msg: 'RepTable has wrong structure', data: {tableName: tableName, dbName: dbName}})
            }

          }
          catch(e) {}

          var tableJSON = getTableStructureFromRange(sheet.getRange(range), virtualColsPosition, table);

          table.fields = tableJSON.fields;
        }
      }

      if (isRepTable) {
        rep_only_dbs[dbk].tables[tableName] = {
          name: table.name,
          comment: table.comment,
          link: table.link,
          fields: table.fields,
        };
      }
    }

    if (!hasReptable) {
      delete rep_only_dbs[dbk];
    }
  }

  // Save current to cache file for next reading speed boot
  if (table_was_edited) {
    saveDbMapCache(JSON.stringify(dbs));
  }

  // Scan all table to get a `table -> field` map. and btw, get indexesMap also (we save some additional loop)
  var tableToFieldsMap = {}; // tableToFieldsMap[tableName][fieldName] = (json in K column)
  var indexesMap = {};
  for(var dbk in dbs) {
    var db = dbs[dbk];
    var dbName = db.name;
    var tables = db.tables;

    indexesMap[dbName] = {};

    for (var tableName in tables) {
      var table = tables[tableName];
      var isRepTable = tableName.indexOf(REPLICATION_TABLE_PREFIX) === 0;
      tableToFieldsMap[tableName] = {};
      indexesMap[dbName][tableName] = [];


      if (isRepTable) {
        Logger.log({})
        Logger.log({isRepTable:isRepTable, tableName:tableName})

        // Scan rep_* tables only
        for (var fieldName in table.fields) {
          var field = table.fields[fieldName];

          Logger.log({needIndex:field.needIndex, fieldName:fieldName})

          if (field.needIndex == true) {
            indexesMap[dbName][tableName].push(fieldName);
          }
        }

      } else {
        Logger.log({})
        Logger.log({isRepTable:isRepTable, tableName:tableName, dbName:dbName})

        // Scan on non-rep_* tables only
        for (var fieldName in table.fields) {
          var field = table.fields[fieldName];
          if (field.repInfo != null && Object.keys(field.repInfo).length > 0) {
            tableToFieldsMap[tableName][fieldName] = field.repInfo;

            // If dst_field_indexes is array then push some nested index
            var dst_field_indexes = field.repInfo.dst_field_indexes;
            if (typeof dst_field_indexes === 'object') {
              for (var idxField in dst_field_indexes) {
                indexesMap[dbName][tableName].push(fieldName + '.' + dst_field_indexes[idxField]);
              }
            }
          }

          Logger.log({needIndex:field.needIndex, fieldName:fieldName})

          if (!CREATE_REP_INDEXES_ONLY && field.needIndex == true) {
            indexesMap[dbName][tableName].push(fieldName);
          }
        }
      }

      if (Object.keys(tableToFieldsMap[tableName]).length == 0) {
        delete tableToFieldsMap[tableName];
      }

      if (indexesMap[dbName][tableName].length == 0) {
        delete indexesMap[dbName][tableName];
      }
    }
  }


  // Get map for Table -> Table: rep_only_dbs
  for(var rep_dbk in rep_only_dbs) {
    var rep_dbk = rep_only_dbs[rep_dbk];
    var rep_dbName = rep_dbk.name;
    var rep_tables = rep_dbk.tables;

    for (var rep_tableName in rep_tables) {
      var rep_table = rep_tables[rep_tableName];
      var source_table_name = rep_table.name.replace(/^rep_(.+)/, "$1");

      var source_table_path = table_path[source_table_name];
      var rep_table_path = rep_dbName + '.' + rep_tableName;

      if (typeof schema_map[source_table_path] === 'undefined') {
        schema_map[source_table_path] = {}
      }

      schema_map[source_table_path][rep_table_path] = Object.keys(rep_table.fields).length > 0 ? Object.keys(rep_table.fields) : []; // Default is empty array
    }
  }


  /* Get map for Table -> Field: "roma_job.job": The JSON from K column  */
  for(var tableName in tableToFieldsMap) {
    var jsonList = tableToFieldsMap[tableName];

    for(var field in jsonList) {
      var json = jsonList[field];
      var source_table_path = table_path[json.join_src_table];
      var dst_table_path = table_path[tableName];

      Logger.log({src: json.join_src_table, dst:tableName, source_table_path: source_table_path, dst_table_path: dst_table_path})

      if (typeof schema_map[source_table_path] === 'undefined') {
        schema_map[source_table_path] = {}
      }

      schema_map[source_table_path][dst_table_path] = json;
    }
  }


  var listenedTables = getListenedInfo(schema_map);
  var createIndexStr = getCreateIndexStr(indexesMap);


  // Send mail to whom run the script
  var recipient = Session.getActiveUser().getEmail();
  var subject = '[Google App Script] Generate schema map log content';
  var body = Logger.getLog();
  MailApp.sendEmail(recipient, subject, body);


  // Return data to view
  return {
    schema_map: schema_map,
    listenedTables: listenedTables,
    createIndexMap: indexesMap,
    createIndexStr: createIndexStr,
  };
}


function getExportData_SchemaMap_FromCache() {
  var schemaMapCache = readDbMapCache();

  // Init empty schema_map:
  var dbs = schemaMapCache.data;
  var schema_map = {};
  var rep_only_dbs = {}; // Db that contain `rep_` tables only
  var table_path = {};  // Build index: [table_name => db_name.table_name]
  var table_was_edited = false;

  var count = 0;
  // Async process: read tables and store definition in dbs
  for(var dbk in dbs) {
    var db = dbs[dbk];
    var dbName = db.name;
    var tables = db.tables;

    rep_only_dbs[dbk] = {
      name: db.name,
      comment: db.comment,
      tables: {}
    };

    var hasReptable = false;

    for (var tableName in tables) {
      count++;

      //// Only care about rep_* tables <=> tableName not start with `rep_`
      var isRepTable = tableName.indexOf(REPLICATION_TABLE_PREFIX) === 0;

      if (!isRepTable) {
        table_path[tableName] = dbName + '.' + tableName;
      //  continue; // We still need to read non rep_table, so do not `continue` right now.
      } else {
        hasReptable = true;
      }

      var table = tables[tableName];

      if (isRepTable) {
        rep_only_dbs[dbk].tables[tableName] = {
          name: table.name,
          comment: table.comment,
          link: table.link,
          fields: table.fields,
        };
      }
    }

    if (!hasReptable) {
      delete rep_only_dbs[dbk];
    }
  }





//  // Save current to cache file for next reading speed boot
//  if (table_was_edited) {
//    saveDbMapCache(JSON.stringify(dbs));
//  }




  // Scan all table to get a `table -> field` map. and btw, get indexesMap also (we save some additional loop)
  var tableToFieldsMap = {}; // tableToFieldsMap[tableName][fieldName] = (json in K column)
  var indexesMap = {};
  for(var dbk in dbs) {
    var db = dbs[dbk];
    var dbName = db.name;
    var tables = db.tables;

    indexesMap[dbName] = {};

    for (var tableName in tables) {
      var table = tables[tableName];
      var isRepTable = tableName.indexOf(REPLICATION_TABLE_PREFIX) === 0;
      tableToFieldsMap[tableName] = {};
      indexesMap[dbName][tableName] = [];


      if (isRepTable) {
        Logger.log({})
        Logger.log({isRepTable:isRepTable, tableName:tableName})

        // Scan rep_* tables only
        for (var fieldName in table.fields) {
          var field = table.fields[fieldName];

          Logger.log({needIndex:field.needIndex, fieldName:fieldName})

          if (field.needIndex == true) {
            indexesMap[dbName][tableName].push(fieldName);
          }
        }

      } else {
        Logger.log({})
        Logger.log({isRepTable:isRepTable, tableName:tableName, dbName:dbName})

        // Scan on non-rep_* tables only
        for (var fieldName in table.fields) {
          var field = table.fields[fieldName];
          if (field.repInfo != null && Object.keys(field.repInfo).length > 0) {
            tableToFieldsMap[tableName][fieldName] = field.repInfo;

            // If dst_field_indexes is array then push some nested index
            var dst_field_indexes = field.repInfo.dst_field_indexes;
            if (typeof dst_field_indexes === 'object') {
              for (var idxField in dst_field_indexes) {
                indexesMap[dbName][tableName].push(fieldName + '.' + dst_field_indexes[idxField]);
              }
            }
          }

          Logger.log({needIndex:field.needIndex, fieldName:fieldName})

          if (!CREATE_REP_INDEXES_ONLY && field.needIndex == true) {
            indexesMap[dbName][tableName].push(fieldName);
          }
        }
      }

      if (Object.keys(tableToFieldsMap[tableName]).length == 0) {
        delete tableToFieldsMap[tableName];
      }

      if (indexesMap[dbName][tableName].length == 0) {
        delete indexesMap[dbName][tableName];
      }
    }
  }


  // Get map for Table -> Table: rep_only_dbs
  for(var rep_dbk in rep_only_dbs) {
    var rep_dbk = rep_only_dbs[rep_dbk];
    var rep_dbName = rep_dbk.name;
    var rep_tables = rep_dbk.tables;

    for (var rep_tableName in rep_tables) {
      var rep_table = rep_tables[rep_tableName];
      var source_table_name = rep_table.name.replace(/^rep_(.+)/, "$1");

      var source_table_path = table_path[source_table_name];
      var rep_table_path = rep_dbName + '.' + rep_tableName;

      if (typeof schema_map[source_table_path] === 'undefined') {
        schema_map[source_table_path] = {}
      }

      schema_map[source_table_path][rep_table_path] = Object.keys(rep_table.fields).length > 0 ? Object.keys(rep_table.fields) : []; // Default is empty array
    }
  }


  /* Get map for Table -> Field: "roma_job.job": The JSON from K column  */
  for(var tableName in tableToFieldsMap) {
    var jsonList = tableToFieldsMap[tableName];

    for(var field in jsonList) {
      var json = jsonList[field];
      var source_table_path = table_path[json.join_src_table];
      var dst_table_path = table_path[tableName];

      Logger.log({src: json.join_src_table, dst:tableName, source_table_path: source_table_path, dst_table_path: dst_table_path})

      if (typeof schema_map[source_table_path] === 'undefined') {
        schema_map[source_table_path] = {}
      }

      schema_map[source_table_path][dst_table_path] = json;
    }
  }

  var listenedTables = getListenedInfo(schema_map);
  var createIndexStr = getCreateIndexStr(indexesMap);


  // Send mail to whom run the script
  var recipient = Session.getActiveUser().getEmail();
  var subject = '[Google App Script] Generate schema map log content (from cache)';
  var body = Logger.getLog();
  MailApp.sendEmail(recipient, subject, body);


  // Return data to view
  return {
    schema_map: schema_map,
    listenedTables: listenedTables,
    createIndexMap: indexesMap, // TODO: Use a function to make indexes for re-use purpose
    createIndexStr: createIndexStr,
  };
}


/**
* Return JSON structure
{
  [dbName:string] : {
    name: string,
    comment: string,
    tables: {
      name: string,
      comment: string,
      link: string,
      fields: {},
    }
  }
}
*/
function getDbListFromRange (range, fieldsPosition) {
  var rangeValues = range.getValues();
  //var rangeLines = range.getFontLines();

  var schemaMapCache = readDbMapCache();

  Logger.log({cacheLoaded: schemaMapCache.last_update_str})

  var dbs = {};
  var currentDbName = '';

  var curRowIndex = 0;
  rangeValues.forEach(function (row){
    var rowObj = _mapRowToObj(row, fieldsPosition);

    // If row start a new table, then create new table
    var newDbFound = rowObj.dbName.length > 0;
    var newTableFound = rowObj.tableName.length > 0;

    if (newDbFound){
      // Create new db
      var tmpStr = rowObj.dbName.trim();
      currentDbName = tmpStr.replace(/(.+) \((.+)\)$/, "$2");
      var comment = tmpStr.replace(/(.+) \((.+)\)$/, "$1");

      // If regex failed then both currentDbName & coment was tmpStr.
      if (currentDbName !== comment) {
        dbs[currentDbName] = {
          name: SCHEMA_PREFIX + currentDbName,
          comment: comment,
          tables: {},
      }
    }
  } else if (newTableFound) {
    var tableName = rowObj.tableName.trim();
    dbs[currentDbName]['tables'][tableName] = {
      name: tableName,
      comment: rowObj.tableComment,
      link: rowObj.tableLink,
      fields: {},
      cache_expired: hasTableBeenEdited(schemaMapCache, tableName, rowObj.tableLink),
    }
  }

  curRowIndex ++;
});

return {
  dbs: dbs,
  schemaMapCache: schemaMapCache,
};
}

/*
* return {
  name: string,
  comment: string,
  fields: {
    fieldName: fieldObj
  }
}
*/
function getTableStructureFromRange (range, fieldsPosition, option) {
  var rangeValues = range.getValues();
  //var rangeLines = range.getFontLines();

  var table = {
    name: 'Plz get from sheet index',
    comment: 'Plz get from sheet index',
    fields: {},
  };

  var curRowIndex = 0;
  var continuosEmptyRowCount = 0;

  var BreakException = {};
  for (var i = 0, c = rangeValues.length; i < c ; i++) {
    var rowObj = _mapRowToObj(rangeValues[i], fieldsPosition);

    if (typeof rowObj.name === 'number') {
      throw new Error("[" + JSON.stringify(option) + "] typeof field name = '" + typeof rowObj.name + "' is not correct, please check the table: " + JSON.stringify(rowObj));
    }

    if (rowObj.name.trim().length > 0) {
      continuosEmptyRowCount = 0;

      var fieldName = rowObj.name;
      var fieldType = rowObj.type;
      var fieldCmt = rowObj.comment + (rowObj.desc.length ? " \n==> " + rowObj.desc : '');
      var repInfo = null;
      var needIndex = typeof rowObj.needIndex == 'boolean' ? rowObj.needIndex : false;

      if (rowObj.repInfo.length) {
        try {
          // Remmember to remove control character before parse JSON
          repInfo = JSON.parse(removeInvalidJsonChars(rowObj.repInfo));
        } catch (e) {
          // Logs an ERROR message.
          var msgObj = {err: e.message, row: rowObj, additionInfo: option};
          Logger.log(msgObj);
          throw new Error("Invalid " + option.name + " specification, plz check your spec doc structure: " + JSON.stringify(msgObj));
        }
      }

      if (fieldType.length > 0) {
        fieldName = toDbFieldNameStandard(fieldName);
        table.fields[fieldName] = {
          name: fieldName,
          type: fieldType,
          comment: fieldCmt,
          needIndex: needIndex,
          repInfo: repInfo,
        }
      }
    }
    else {
      // NOTE: To debug, ensure to edit your table spec, so your table was ignore to load from cache.
      continuosEmptyRowCount++;

      // break if 3 continous rows are all empty
      if (continuosEmptyRowCount >= 3) {
        break;
      }
    }

    curRowIndex ++;
  }

  return table;
}

function getCreateIndexStr(indexesMap) {
  var createIndexStr = '';
  var defaultIndexes = DEFAULT_INDEXES_FIELDS.map(function (e) {var o={}; o[e] = 1; return o;});
  var defaultIdxFields = ArrayUtil__toObject(DEFAULT_INDEXES_FIELDS, function (i) {return i}, function (i) {return true});

  for(var dbName in indexesMap) {
    var db = indexesMap[dbName];
    createIndexStr += "\n// ======== db: " + dbName + " ========\n" +
      "use " + dbName +"\n";

    for(var tableName in db) {
      var idx = [];
      var tmpIdxForCode = '';

      for(var i in db[tableName]) {
        var fieldName = db[tableName][i];

        /*
         * Handle for code
         */
        if (fieldName == 'code') {
          tmpIdxForCode = "db.getCollection('"+ tableName +"').createIndex({code: 1}, {unique: true});\n";
        }
        /*
         * Handle for default index
         */
        else if (typeof defaultIdxFields[fieldName] !== 'undefined') {
          // If is default Indexes --> Do nothing because this idx def was created later
        }
        else {
          var obj = {};
          obj[fieldName] = 1;
          idx.push(obj);
        }
      }

      var tmpIdxes = "db.getCollection('"+ tableName +"').createIndexes("+ JSON.stringify(ArrayUtil__merge(idx, defaultIndexes)) +");\n";

      createIndexStr += tmpIdxForCode + tmpIdxes;
    }
  }

  return createIndexStr;
}

function getListenedInfo(schema_map) {
  // Get listened field from schema map
  var listenedFields = {
    // table: {field1: true, field3: true, field4: true}, // only list the "be listened field"
  }
  // Get listenedTableCsv
  var listenedTables = {
    db: {}, // List of db need to listen on
    table: {}, // List of table need to listen on
    field: [], // List of field need to listen on | element = "table.field"
  };
  for(var source_table_path in schema_map) {
    var tmp = source_table_path.split('.');
    var db = tmp[0];
    var tb = tmp[1];

    listenedTables.db[db] = db;
    listenedTables.table[tb] = tb;

    // go deep inside to get "be listened fields"
    if (typeof listenedFields[tb] === 'undefined') {
      listenedFields[tb] = {}
    }
    var dst_info = schema_map[source_table_path];
    for(var dst_table in dst_info) {
      var dst_conf = dst_info[dst_table];
      if (Array.isArray(dst_conf)) {
        // dst_conf is array -> case: table2table
        for(var i in dst_conf) {
          var field = dst_conf[i];
          listenedFields[tb][field] = true;
        }
      } else {
        // dst_conf is object -> case: table2fields
        for(var i in dst_conf.src_fields) {
          var field = dst_conf.src_fields[i];
          listenedFields[tb][field] = true;
        }
      }
    }
  }
  listenedTables.db =  Object.keys(listenedTables.db);
  listenedTables.table =  Object.keys(listenedTables.table);

  for (var table in listenedFields) {
    for (var field in listenedFields[table]) {
      listenedTables.field.push(table + '.' + field);
    }
  }

  return listenedTables;
}

var __indexOf = __getIndexOfMemoizeFromColumn("A");
/*
NOTE: If you change column structure in excel, remember to change this IDX
*/
function _mapRowToObj(arr, cols) {
  var obj = {};
  for (var k in cols) {
    obj[k] = arr[__indexOf(cols[k])];
  }

  return obj;
}

/**
* This function is memoization function, so you must understanding memoize before use it.
*
* Use for this file only:
*
* D --> 0
* E --> 1
* F --> 2
* @param col column name Eg: A
*/
function __getIndexOfMemoizeFromColumn(startCol) {
  var cache = {};
  var FIRST_COL_CODE = startCol.charCodeAt(0);

  return function (col) {
    if (typeof cache[col] !== 'undefined') {
      return cache[col];
    }
    else {
      cache[col] = col.charCodeAt(0) - FIRST_COL_CODE;
      return cache[col];
    }
  }
}

function _addTableField(tableRef, rowObj) {
  tableRef.fields[rowObj.field] = {
    name: rowObj.field,
    type: rowObj.fieldType,
    comment: rowObj.fieldNote,
    mapped: rowObj.fieldMapped,
  }
}
