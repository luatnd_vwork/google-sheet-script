# excel-script-tools
This repos includes Google Sheet plugins to:
- generate SCHEMA_MAP json for kafka-to-mongo from ~30 gsheet
- generate MySQL database query from >100 table specification gsheet files
    - Use this SQL to create your project database schema.
    - Output *.sql files into a Google Driver folder.

Edit, Run, Debug online here:

Google App Script project: [Excel table to GDriver sql files](https://script.google.com/macros/d/MaKJmtJsOChaYQq4E2mgxbEtKlU4A6qcz/edit?uiv=2&mid=ACjPJvHNkguz-nTu2Nte-73guYdseIOgyFidwLPPrTYqTFIQzFWRHCE5QVviqFxHETQ7KNAgQ0eW5AM3zisqo_8-yVmNU-wllulTMEAU4MNe0JH8lvO_GQBfc-8tNqLALE9n8XVZ9uyEYQk)


## Google App Script Introduction
**Overview**

Google let you do anything with doc, sheet, driver via Google App Script language. Some use cases:
* Create your custom GSheet function
* Write an web app (plugin) for GSheet, using driver + docs + sheet + firebase as your database.
* Everything else


**Power up your Google Sheet**

If you wanna create a menu, and program sth like this repo. See this guide first:

https://developers.google.com/apps-script/guides/sheets

- Get started
- Reading data
- Writing data
- Custom menus and user interfaces
- Connecting to Google Forms
- Formatting
- Data validation
- Triggers
- Charts
- Custom functions in Google Sheets
- Macros
- Add-ons for Google Sheets


## How to develop: 1. Structure
#### 1_ Background

A gsheet extension (or a bigger app) was written in Google App Script:
* GAS syntax very similar to Javascript ES5
* GAS design very similar to PHP: Request -> Run -> Destroy process
* GAS was run on Google server, not browser
* Familiar with anyone who have already a web dev


#### 2_ Script editor (online lite IDE)


#### 3_ App structure,

**Entry point**

Imagine in PHP, we have an index.php entry point file.
In GAS, `code.gs` is the entrain to the app.

**Controller**

**View**

**Datasource**


#### 3_ Some Gotcha

- GAS is very early bird stage, so it very simple
- Poor OOP --> Because of tiny app size, functional programming is recommend instead of OOP
- Source code requiring like PHP require_one, include_one. And was done automatically. You don't need to type include anymore.
- Global, no name-space. Just define a function in a file, then call it at another file, it's ok.
- Can not create folder, your app dir is definitly flat!

#### 3_ Debugging

- You can set breakpoint and debug like other IDE.
- try..catch similar to Javascript:
    ```
    try {
        doSthWrong();
    } catch(err) {
        throw new Error("foo bar")
    }
    ```

#### 3_ Logging
```javascript
Logger.log({foo: "bar"}); // GAS will hightlight when you type!
```
Then select menu View -> Log (or Cmd + Enter) to see the log.

#### 3_ Interact with gsheet, gdocs, gdriver
Just Google it, GAS can do everything with it sibling products in Google ecosystem.

For example:
- Read excel file by ID or from url, see here: [Utils/Sheet.gs](Utils/Sheet.gs)
- Create and Save file to GDriver: [AppCtrlSaveSql.gs](AppCtrlSaveSql.gs)

