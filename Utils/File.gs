function createNewDriverFile(folder, fileName, content) {
  folder.createFile(fileName, content);
}

function readJsonByFileId(driverFileId) {
  var text = readTextFileById(driverFileId);
  return JSON.parse(text);
}

function readTextFileById(driverFileId) {
  var f = DriveApp.getFileById(ID);
  var plaintext = f.getAs('text/plain');
  return plaintext;
}

function getLastUpdatedById(driverFileId) {
  var f = DriveApp.getFileById(driverFileId);
  var date = f.getLastUpdated();
  return date;
}

function getFileIdFromSheetUrl(sheetUrl) {
  if (sheetUrl.length == 0) {
    return null;
  }

  var ID = null;


  // case drive.google.com:
  // https://drive.google.com/open?id=14kpl7yM9wrq3OOZdmyEybIlFGbpHNMVkiYApAHLTF7A
  var matched = sheetUrl.match(/\/open\?id=(.+)$/);
  if (matched !== null) {
    var ID = matched[1];
  }

  // case: docs.google.com
  // https://docs.google.com/spreadsheets/d/1jZHGTIEhJCFSW8Zr5PVph_anMWN8F1diR6VnWFVsfEE/edit#gid=993862298
  // https://docs.google.com/spreadsheets/u/3/d/1WuvPay7Sh-rMbdcYOiyrby6dKTGe5ximvOgLuE1zSNU/edit?usp=drive_web&ouid=114251781062537963060
  if (ID === null) {
    var matched = sheetUrl.match(/\/spreadsheets(\/.+)?\/d\/([^\/]+)\//);
    if (matched !== null) {
      var ID = matched[2];
    }
  }

  if (ID !== null) {
    return ID
  } else {
    return null
  }
}


function test__File () {
  var a1 = getFileIdFromSheetUrl('https://drive.google.com/open?id=14kpl7yM9wrq3OOZdmyEybIlFGbpHNMVkiYApAHLTF7A');
  var a2 = getFileIdFromSheetUrl('https://docs.google.com/spreadsheets/d/1jZHGTIEhJCFSW8Zr5PVph_anMWN8F1diR6VnWFVsfEE/edit#gid=993862298');
  var a3 = getFileIdFromSheetUrl('https://docs.google.com/spreadsheets/u/3/d/1WuvPay7Sh-rMbdcYOiyrby6dKTGe5ximvOgLuE1zSNU/edit?usp=drive_web&ouid=114251781062537963060');
  var a4 = getFileIdFromSheetUrl('https://docs.google.com/spreadsheets/u/1/d/1ZheJl9yVkvux8ky0cjT4Km4peHbgiaUHemUEydmNdVI/edit');
  var a5 = getFileIdFromSheetUrl('https://docs.google.com/spreadsheets/u/1/d/1KQcC0HnlYWWEiv6bZYxb4FFm3n7G_2gTwvW7-5KI5Cs/edit?usp=drive_web&ouid=113739971446489598064');

  var stop = true;
}