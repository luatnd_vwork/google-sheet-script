/**
 * Reading ~200 tables by their url is very slow, so we cache table info until someone edit the sheet, then we refetch this table cache.
 */
var SCHEMA_MAP_CACHE_FOLDER_ID = '1CDdoLYi494odxpwHKNPw5rs0QLwyUrpE'; // https://drive.google.com/drive/u/2/folders/1CDdoLYi494odxpwHKNPw5rs0QLwyUrpE
var CACHE_FILE_SUFFIX = 'dbs_map.json';

function test__Cache() {
  var sheetUrl = 'https://docs.google.com/spreadsheets/d/1sCWlhX71v68HuksY-2ob9cjwmA9kzB_lZ9gfDs9SHCE/edit#gid=993862298';
  var a = hasTableBeenEdited({
    last_update_str: "180601010101",
    data: {},
  }, 'test_tb', sheetUrl);

  var stop = true;
}

function hasTableBeenEdited(schemaMapCache, tableName, sheetUrl) {
  var ID = getFileIdFromSheetUrl(sheetUrl);
  if (ID == null) {
    return true;
  }

  var last_update = getLastUpdatedById(ID);
  var last_update_str = Utilities.formatDate(last_update, "GMT+7", "yyMMddHHmmss");
  var edited = (last_update_str > schemaMapCache.last_update_str);

  Logger.log({
    msg: 'Table ' + tableName + (edited ? ' has been edited': ' unchanged'),
    last_update: last_update_str,
    cache_update: schemaMapCache.last_update_str
  })

  return edited;
}


function readDbMapCache() {
  Logger.log({"cache_folder": 'https://drive.google.com/drive/u/2/folders/1CDdoLYi494odxpwHKNPw5rs0QLwyUrpE'})

  var folder = DriveApp.getFolderById(SCHEMA_MAP_CACHE_FOLDER_ID);
  var files = folder.getFiles();

  var latest = "180601010101"; // 2018-06-01 01:01:01
  while (files.hasNext()) {
    var file = files.next();
    var last_update_str = file.getName().replace(/^([0-9]+)_.*/, '$1'); // file name rule: 180601010101_dbs_map.json
    if (last_update_str > latest) {
      latest = last_update_str;
    }
  }

  var files = folder.getFilesByName(latest + '_' + CACHE_FILE_SUFFIX);
  while (files.hasNext()) {
    var file = files.next();
    var jsonFile = file.getAs('application/json');
    var jsonData = JSON.parse(jsonFile.getDataAsString());

    // Return first file found
    return {
      last_update_str: latest,
      data: jsonData,
    }
  }

  return {
    last_update_str: "",
    data: {},
  }
}


function saveDbMapCache(content) {
  var SCHEMA_MAP_PREFIX = Utilities.formatDate(new Date(), "GMT+7", "yyMMddHHmmss");

  var fileName = SCHEMA_MAP_PREFIX + '_' + CACHE_FILE_SUFFIX;
  var folder = DriveApp.getFolderById(SCHEMA_MAP_CACHE_FOLDER_ID);

  folder.createFile(fileName, content);
}

