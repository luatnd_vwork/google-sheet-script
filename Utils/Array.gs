function ArrayUtil__toObject(arr, keyGetter, valueGetter) {
  const obj = {};

  for (var i in arr) {
    var e = arr[i];
    obj[keyGetter(e)] = valueGetter(e);
  }

  return obj;
}


function ArrayUtil__merge(a1, a2){
  for (var i in a2) {
    a1.push(a2[i]);
  }

  return a1;
}
