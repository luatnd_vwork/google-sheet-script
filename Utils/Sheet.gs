/*
* Get sheet by sheet index in a URL
* @param sheeUrl
* @param sheetZeroBasedIndex First sheet is 0
*
* NOTE: Use link doc instead of driver:
*    Valid url:   https://docs.google.com/spreadsheets/d/1sCWlhX71v68HuksY-2ob9cjwmA9kzB_lZ9gfDs9SHCE/edit#gid=993862298
*    Invalid url: https://drive.google.com/open?id=1sCWlhX71v68HuksY-2ob9cjwmA9kzB_lZ9gfDs9SHCE
*/
function getNetworkSheet(sheetUrl, sheetZeroBasedIndex) {
  var ID = getFileIdFromSheetUrl(sheetUrl);

  Logger.log({action: 'getNetworkSheet', fileID: ID, url: sheetUrl});

  // Try to read by file ID (very fast) so that google don't need to fetch file via slow HTTP.
  if (ID !== null) {
    var ss = SpreadsheetApp.open(DriveApp.getFileById(ID));
    var sheet = ss.getSheets()[sheetZeroBasedIndex];

    return sheet;
  }

  // Fallback: if can not read by ID then read by url
  var ss = SpreadsheetApp.openByUrl(sheetUrl);
  var sheet = ss.getSheets()[sheetZeroBasedIndex];

  return sheet;
}
